<?php
/*
Plugin Name: Student Charts
Plugin URI: https://bitbucket.org/sf-repos/wordpress-student-score-chart-plugin
Description: Display charts that display using a shortcode for a class. This uses the data from the custom post types ("Classes" and "Students") included in this plugin.
Version: 1.0
Author: Sam Fullalove
Author URI: http://sam.fullalove.co
Text Domain: sjf-student-charts
License: GPLv2
*/

/*
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
USA
*/


/**
* Run this code when plugin is activated
*/
function sjf_student_charts_activation() {
  global $wp_version;

  //make sure running minimum wordpress version (that I've tested) exists to use correct wp functions
  if ( version_compare( $wp_version, '5.2', '<' ) ) {
  wp_die( 'This plugin requires WordPress version 5.2 or higher.' );

  }
}
register_activation_hook( __FILE__, 'sjf_student_charts_activation' );

/**
 * Enqueue css
 */
function sjf_student_charts_scripts() {
  $post = get_post();

  //if the current page is using the shortcode
  if ( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'sjf_student_charts') ) {
  //include the google chart javascript
    wp_register_script( 'sjf_student_charts_gcharts', 'https://www.gstatic.com/charts/loader.js' , array(), false ,false );
    wp_enqueue_script( 'sjf_student_charts_gcharts' );
   //include the css
    wp_register_style( 'sjf_student_charts_css', plugin_dir_url( __FILE__ ) . 'css/style.css', false, '1.0.0' );
    wp_enqueue_style( 'sjf_student_charts_css' );
  }
}
add_action( 'wp_enqueue_scripts', 'sjf_student_charts_scripts', 11 );

//include the CPTs required
include_once('includes/custom-post-types.php');

/**
* [sjf_student_charts className=XXXX] Shortcode for displaying all of the frontend html and js
*/
function sjf_student_charts_shortcode($className){


  global $wpdb;
  $className = $className['classname'];

  //find the class' ID
  $classID = $wpdb->get_row( 
    'SELECT id
    FROM wp_posts
    WHERE post_title LIKE "'.$className.'" && post_type = "classes"');

  wp_reset_postdata();

  $classID = $classID->id;

  //find the students
  $students = $wpdb->get_results( 
  'SELECT wp_posts.id, wp_posts.post_title
  FROM wp_posts
  INNER JOIN wp_postmeta ON wp_posts.ID=wp_postmeta.post_id
  WHERE wp_postmeta.meta_key = "class" && wp_postmeta.meta_value = '.$classID);

  wp_reset_postdata();

  //create array and get the students score
  $studentsAndScores = array(
    array(
      '',
      0,
      0,
    )
  );
  $x = 0;

  while (count($students) > $x){
    $studentsAndScores[$x][0] = $students[$x]->post_title;
    $studentsAndScores[$x][1] = get_post_meta( $students[$x]->id, 'score', true );
    $studentsAndScores[$x][2] = $students[$x]->id;
    $x++;
  }

  return '
  <div id="gauge_div" style="width:100%; height: 200px;"></div>

  <div id="updateAllStudents">
      <input id="goodNameButton" type="button" value="All Good Names" onclick="updateAllStudents('.$classID.',1)" />
      <input id="badNameButton" type="button" value="All Bad Names" onclick="updateAllStudents('.$classID.',-1)" />
  </div>

  <br/><br/>
  <div id="studentButtons"></div> 

  <script type="text/javascript">
  var names = '.json_encode( $studentsAndScores ).'
  '."
  
 ;
  google.charts.load('current', {'packages':['gauge']});
  google.charts.setOnLoadCallback(drawGauge);

  var gaugeOptions = {min: 0, max: 10, yellowFrom: 6, yellowTo: 8,
    redFrom: 8, redTo: 10, minorTicks: 5};
  var gauge;
  
  var numOFLoops = names.length;

  /**
  * Update all the student's scores by 1 or -1
  * @param int $classID The ID of the class meta data
  * @param int $val amount to alter the score by 1 or -1
  */
  function updateAllStudents(classID, val){
    var x = 0;

    while (x < numOFLoops)
    {
        gaugeData.setValue(0, x, gaugeData.getValue(0, x) + val );  
        x++;
    }

    gauge.draw(gaugeData, gaugeOptions);

    jQuery.ajax({
      url: `".site_url()."/wp-content/plugins/sjf-student-charts/includes/updateDb.php`,
      type: 'POST',
      dataType: \"json\",
      data: {
          classID: classID,
          score: val,
      }
  }).done(function(data){
          
  });
}

  /**
  * Update all the student's scores by 1 or -1
  * @param int $studentID The ID of the class meta data
  * @param int $gaugeNum The gauge to update
  * @param int $val amount to alter the score by 1 or -1
  */
  function updateSingleStudent(studentID, gaugeNum , val){
     gaugeData.setValue(0, gaugeNum, gaugeData.getValue(0, gaugeNum) + val ); 
     gauge.draw(gaugeData, gaugeOptions);

     jQuery.ajax({
      url: `".site_url()."/wp-content/plugins/sjf-student-charts/includes/updateDb.php`,
      type: 'POST',
      dataType: \"json\",
      data: {
          studentID: studentID,
          score: val,
      }
      }).done(function(data){
      });

  }
  
  /**
  * Display the gauges initially 
  */
  function drawGauge() {
      var x = 0;

      gaugeData = new google.visualization.DataTable();
      gaugeData.addRows(numOFLoops);

      while (x < numOFLoops)
      {
          gaugeData.addColumn('number', names[x][0]);
          gaugeData.setCell(0, x, names[x][1]);

          x++;
      }

      gauge = new google.visualization.Gauge(document.getElementById('gauge_div'));
      gauge.draw(gaugeData, gaugeOptions);
  }
  
  /**
  * Display the buttons
  */
  function displayStudentButtons(){
      var buttonHtml = '';
      var x = 0;

      while (x < numOFLoops)
      {   
    ".'
      //create button HTML to display
      buttonHtml += `<div id="studentButtonsInner">`;
      buttonHtml += `<button id="goodNameButton" type="submit" onclick="updateSingleStudent(${names[x][2]}, ${x},1)"> ${names[x][0]} <i class="fa fa-thumbs-up" aria-hidden="true"></i></button> `;
      buttonHtml += `<button id="badNameButton" type="submit" onclick="updateSingleStudent(${names[x][2]}, ${x},-1)"> ${names[x][0]} <i class="fa fa-thumbs-down" aria-hidden="true"></i></button> `;
      buttonHtml += `</div>`;
      
      x++;
      }
    
      document.getElementById("studentButtons").innerHTML = buttonHtml;
  }
  
  displayStudentButtons();
  
  </script>

  <span>You must be logged in to use the buttons</span>
  ';

}
add_shortcode( 'sjf_student_charts', 'sjf_student_charts_shortcode' );