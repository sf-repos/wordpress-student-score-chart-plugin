# Student Charts

This a wordpress plugin which allows teachers to track multiple classes and student's score out of ten.

After installing, you can add classes to the admin area of wordpress. 

Next you can add students and assign them to a class.

Finally you can use the shortcode [sjf_student_charts className=XXXX] where XXXX is the name of the class.

If you are logged in as an admin, you can add and subtract points to the students from the front end. 
If you are not logged in, these points will not be saved to the database.

Classes:

![picture](https://bitbucket.org/sf-repos/wordpress-student-score-chart-plugin/raw/master/screenshots/classes.png)

Students:

![picture](https://bitbucket.org/sf-repos/wordpress-student-score-chart-plugin/raw/master/screenshots/students.png)

Shortcode:

![picture](https://bitbucket.org/sf-repos/wordpress-student-score-chart-plugin/raw/master/screenshots/shortcode.png)

Frontend:

![picture](https://bitbucket.org/sf-repos/wordpress-student-score-chart-plugin/raw/master/screenshots/frontend.png)

Editing a student:

![picture](https://bitbucket.org/sf-repos/wordpress-student-score-chart-plugin/raw/master/screenshots/edit.png)
