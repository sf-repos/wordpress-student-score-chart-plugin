<?php

/************************************* 
 * How to use this API
 * 
 * Ensure the user is logged in as an admin
 * 
 * To update a single student's score set the studentID and score(1/-1) POST variables
 * 
 * To update all the scores for a class, set the classID and score(1/-1) POST variables
 * 
*****************************************/

//require wordpress files
require_once("../../../../wp-load.php");

    // Check user has admin privileges and update a single student's score by 1 or -1
    if ( current_user_can( 'manage_options' ) && isset($_POST['studentID']) && isset($_POST['score']) ) {

        $studentID = sanitize_text_field( $_POST['studentID'] );
        $score= get_post_meta( $studentID, 'score', true );

        if($_POST['score'] == 1){
            $score ++;
        } else {
            $score --;
        }

        echo "id=".$studentID;
        echo "score=".$score;

        update_post_meta( $studentID, 'score', $score );

        echo "Successfully updated Single Student!";

      //Check user has admin privileges and update the class' score by 1 or -1
    } elseif ( current_user_can( 'manage_options' ) && isset($_POST['classID']) && isset($_POST['score'])) { 
        $classID = sanitize_text_field( $_POST['classID'] ); 
        $students = $wpdb->get_results( 
            'SELECT wp_posts.id, wp_posts.post_title
            FROM wp_posts
            INNER JOIN wp_postmeta ON wp_posts.ID=wp_postmeta.post_id
            WHERE wp_postmeta.meta_key = "class" && wp_postmeta.meta_value = '.$classID);
          
        wp_reset_postdata();

        $x = 0;
        while (count($students) > $x){
          $score = get_post_meta( $students[$x]->id, 'score', true );

          if($_POST['score'] == 1){
            $score ++;
          } else {
            $score --;
          }

          update_post_meta( $students[$x]->id, 'score', $score );
          $x++;
        }

        echo "Successfully updated Class!";
        
      //A user without admin privileges
    } else {
        echo "You must be an admin and set the right variables to use this page. If you are a developer, please check the instructions in the updateDB.php file of the student-charts plugin";
    }
