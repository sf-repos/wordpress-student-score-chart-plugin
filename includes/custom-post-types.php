<?php
########################################################################################################################################
//This file creates the CPTs for student and class. It also add the custom meta boxes for the student CPT, which are "class" and "score"
########################################################################################################################################

/**
* Make the class custom post type
*/ 
function sjf_student_charts_create_posttype_class() {
 
  register_post_type( 'Classes',
  // CPT Options
      array(
          'labels' => array(
              'name' => __( 'Classes' ),
              'singular_name' => __( 'Class' )
          ),
          'supports' => array(
            'title',
        ),
          'public' => true,
          'has_archive' => false,
          'rewrite' => array('slug' => 'class'),
      )
  );
}
// Hook for custom post type function
add_action( 'init', 'sjf_student_charts_create_posttype_class' );

/**
* Make the student custom post type
*/ 

function sjf_student_charts_create_posttype_student() {
 
  register_post_type( 'Students',
  // CPT Options
      array(
          'labels' => array(
              'name' => __( 'Students' ),
              'singular_name' => __( 'Student' )
          ),
          'supports' => array(
            'title',
        ),
          'public' => true,
          'has_archive' => false,
          'rewrite' => array('slug' => 'student'),
      )
  );
}
// Hook for custom post type function
add_action( 'init', 'sjf_student_charts_create_posttype_student' );


/*Add the metabox for the member post type*/
function details_add_meta_box() {
  $screens = array( 'students' );
  
  foreach ( $screens as $screen ) {
  
      add_meta_box(
          'details',
          __( 'Details', 'sjf-student-charts' ),
          'details_meta_box_callback',
          $screen
      );
   }
  }
  add_action( 'add_meta_boxes', 'details_add_meta_box' );

/**
 * Prints the box content.
 *
 * @param WP_Post $post The object for the current post/page.
 */
function details_meta_box_callback( $post ) {

// Add a nonce field so we can check for it later.
wp_nonce_field( 'details_save_meta_box_data', 'member_meta_box_nonce' );

/*
 * Use get_post_meta() to retrieve an existing value
 * from the database and use the value for the form.
 */
$value = get_post_meta( $post->ID, 'score', true );
$value2 = get_post_meta( $post->ID, 'class', true );

echo '<label for="score">';
_e( 'Score', 'sjf-student-charts' );
echo '</label> ';
echo '<input type="text" id="score" name="score" value="' . esc_attr( $value ) . '" size="25" />';

echo '<br/><br/><label for="class">';
_e( 'Class', 'sjf-student-charts' );
echo '</label> ';

$post_type_object = get_post_type_object('classes');
        $label = $post_type_object->label;
        $posts = get_posts(array('post_type'=> 'classes', 'post_status'=> 'publish', 'suppress_filters' => false, 'posts_per_page'=>-1));
        echo '<select name="class" id="class">';
        foreach ($posts as $post) {
            echo '<option value="', $post->ID, '"', $value2 == $post->ID ? ' selected="selected"' : '', '>', $post->post_title, '</option>';
        }
        echo '</select>';

}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
 function details_save_meta_box_data( $post_id ) {

 if ( ! isset( $_POST['member_meta_box_nonce'] ) ) {
    return;
 }

 if ( ! wp_verify_nonce( $_POST['member_meta_box_nonce'], 'details_save_meta_box_data' ) ) {
    return;
 }

 if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
    return;
 }

 // Check the user's permissions.
 if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

    if ( ! current_user_can( 'edit_page', $post_id ) ) {
        return;
    }

 } else {

    if ( ! current_user_can( 'edit_post', $post_id ) ) {
        return;
    }
 }

 if ( ! isset( $_POST['score'] ) ) {
    return;
 }

 if ( ! isset( $_POST['class'] ) ) {
  return;
}


 $my_data = sanitize_text_field( $_POST['score'] );
 $my_data2 = sanitize_text_field( $_POST['class'] );

 update_post_meta( $post_id, 'score', $my_data );
 update_post_meta( $post_id, 'class', $my_data2 );
}
add_action( 'save_post', 'details_save_meta_box_data' );